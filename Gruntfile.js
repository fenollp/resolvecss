module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        purifycss: {
            options: {
            },
            target: {
                src: ['index.html'],
                css: [
                    'css/application-fd40b6b1.css',
                    'css/bootstrap-darkly.min.css'
                ],
                dest: 'css/all.css'
            }
        },

    });

    // Load the plugins.
    grunt.loadNpmTasks('grunt-purifycss');

    // Default task(s).
    grunt.registerTask('default', ['purifycss']);
};
